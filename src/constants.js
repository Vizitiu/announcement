export const SET_LOADING = 'SET_LOADING'
export const SET_ERROR = 'SET_ERROR'
export const FETCH_ITEMS = 'FETCH_ITEMS'
export const GET_ITEM = 'GET_ITEM'
