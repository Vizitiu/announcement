import * as firebase from 'firebase'
import 'firebase/auth'

const app = firebase.initializeApp({
    apiKey: "AIzaSyBR47eSKv30UhDpJKs55Bzqke2UVI8IRp4",
    authDomain: "rannouncement.firebaseapp.com",
    databaseURL: "https://rannouncement.firebaseio.com",
    projectId: "rannouncement",
    storageBucket: "rannouncement.appspot.com",
    messagingSenderId: "931475261549",
    appId: "1:931475261549:web:589c0e4c3f91182c9cd764"
});

export default app
