import React, {useState, useEffect} from 'react';
import app from "../../base";

export const AuthContext = React.createContext()

export const AuthState = ({children}) => {
    const [user, setUser] = useState(null)

    useEffect(() => {
        app.auth().onAuthStateChanged(setUser)
    }, [])

    return (
        <AuthContext.Provider value={{
            user
        }}>
            {children}
        </AuthContext.Provider>
    );
};
