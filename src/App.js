import React from 'react'
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Announcement from "./containers/Announcement/Announcement";
import List from "./containers/List/List";
import {Container} from "@material-ui/core";
import AddAnnouncement from "./components/AddAnnouncement/AddAnnouncement";
import ToolbarAction from "./components/ToolbarAction/ToolbarAction";
import Login from "./components/Auth/Login";
import {SignUp} from "./components/Auth/SignUp";
import {PrivateRoute} from "./PrivateRoute";
import {AuthState} from "./context/auth/AuthState";


function App() {
    return (
        <AuthState>
            <Router>
                <ToolbarAction/>
                <Container>
                    <Switch>
                        <Route exact path={'/'} component={List}/>
                        <Route path={'/announcement/:id'} component={Announcement}/>
                        <PrivateRoute exact path={'/add'} component={AddAnnouncement}/>
                        <Route path={'/login'} component={Login}/>
                        <Route path={'/signup'} component={SignUp}/>
                    </Switch>
                </Container>
            </Router>
        </AuthState>
    )
}

export default App
