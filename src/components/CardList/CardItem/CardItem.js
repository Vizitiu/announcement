import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import {Card} from "@material-ui/core";
import {NavLink} from 'react-router-dom'

const useStyles = makeStyles({
        card: {
            width: 305,
            margin: 15
        },
        media: {
            height: 200,
        },
        cardActions: {
            display: 'flex',
            justifyContent: 'space-around'
        }
    })
;

const CardItem = ({title, id, url, price}) => {
    const classes = useStyles();

    return (
        <Card className={classes.card}>
            <NavLink to={`/announcement/${id}`}>
                <CardActionArea>
                    <CardMedia
                        className={classes.media}
                        image={url}
                        title={title}
                    />
                </CardActionArea>
            </NavLink>
            <CardContent>
                <Typography variant="body2" color="textSecondary" component="p">
                    {title}
                </Typography>
            </CardContent>
            <CardActions className={classes.cardActions}>
                <Typography variant="body2" color="textSecondary" component="span">
                    {price}
                </Typography>
                <NavLink to={`/announcement/${id}`}>
                    <Button size="small" color="primary">
                        Посмотреть
                    </Button>
                </NavLink>
            </CardActions>
        </Card>
    );
};

CardItem.propTypes = {
    title: PropTypes.string.isRequired,
    id: PropTypes.any.isRequired,
    url: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired
};

export default CardItem;
