import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import CardItem from "./CardItem/CardItem";


const useStyles = makeStyles({
        listFlex: {
            display: 'flex',
            flexFlow: 'row wrap',
            justifyContent: 'space-around',
            marginTop: 100
        }
    })
;


const CardList = ({items, loading, error}) => {
    const classes = useStyles();

    return (
        <div className={classes.listFlex}>
            {error && <p>Error</p>}
            {loading
                ? <p>Loading...</p>
                : items.map(({id, title, url, price}) => (
                    <CardItem
                        key={id} id={id} title={title}
                        url={url} price={price}
                    />
                ))}
        </div>
    );
};

CardList.propTypes = {
    items: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    error: PropTypes.bool.isRequired,
};

export default CardList;
