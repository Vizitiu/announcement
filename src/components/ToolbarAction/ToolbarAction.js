import React, {useContext} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {AppBar, Toolbar, Typography, IconButton} from '@material-ui/core';
import {ExitToApp, PostAdd, KeyboardBackspace} from '@material-ui/icons';
import {Link, useHistory} from 'react-router-dom'
import app from '../../base'
import {AuthContext} from "../../context/auth/AuthState";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    appBar: {
        position: 'fixed',
        top: 0,
        background: '#1976d2',
        color: '#ffffff'
    },
}));


const ToolbarAction = () => {
    const classes = useStyles()

    const {user} = useContext(AuthContext)
    const history = useHistory()

    const handleLogout = () => {
        const confirm = window.confirm('Вы уверены что хотите выйти ?')
        if (confirm) app.auth().signOut()
    }

    return (
        <AppBar position="static"
                color={'inherit'}
                className={classes.appBar}>
            <Toolbar>
                <IconButton
                    edge="start"
                    className={classes.menuButton}
                    color="inherit"
                    aria-label="open drawer"
                    onClick={() => history.push('/')}
                >
                    <KeyboardBackspace fontSize={"large"}/>
                </IconButton>
                <Typography className={classes.title} variant="button" noWrap>
                    <IconButton
                        edge="start"
                        className={classes.menuButton}
                        color="inherit"
                        aria-label="open drawer"
                    >
                        <Link to={'/add'}><PostAdd fontSize={"large"}/></Link>
                    </IconButton>
                </Typography>
                {user
                    ? <IconButton
                        edge="start"
                        className={classes.menuButton}
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleLogout}
                    >
                        <ExitToApp fontSize={"large"}/>
                    </IconButton>
                    : null
                }
            </Toolbar>
        </AppBar>
    )
}

export default ToolbarAction;
