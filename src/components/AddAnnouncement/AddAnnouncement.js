import React, {useState} from 'react';
import axios from 'axios'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";

const AddAnnouncement = () => {
    const [values, setValue] = useState({
        title: '',
        body: '',
        price: '',
        region: '',
        url: '',
        username: '',
        contact: '',
    })

    const handleChange = e => {
        const {name, value} = e.target

        setValue({
            ...values,
            [name]: value,
        })
    }

    const handleSubmit = async e => {
        e.preventDefault()

        if (values.title.trim() === '' || values.body.trim() === ''
            || values.price.number === '' || values.region.trim() === ''
            || values.url === '' || values.contact === '' ||
            values.username === '') return alert('Заполните все поля')

        if (!!values) {
            try {
                await axios.post('https://rannouncement.firebaseio.com/items.json', values)
            } catch (e) {
                alert(e)
            }

            setValue({
                title: '',
                body: '',
                price: '',
                region: '',
                url: '',
                username: '',
                contact: '',
            })
        }

    }

    return (
        <Box bgcolor="white" mt={'15vh'} p={'1rem'}>
            <Typography variant="h6">
                Добавить объявление
            </Typography>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <TextField
                        required
                        label="Название"
                        fullWidth
                        name="title"
                        value={values.title}
                        onChange={handleChange}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        label="Описание"
                        name="body"
                        value={values.body}
                        onChange={handleChange}
                        fullWidth
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        label="Цена"
                        name="price"
                        value={values.price}
                        onChange={handleChange}
                        fullWidth
                    />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                        required
                        label="Город"
                        name="region"
                        value={values.region}
                        onChange={handleChange}
                        fullWidth
                    />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                        required
                        label="Имя"
                        name="username"
                        value={values.username}
                        onChange={handleChange}
                        fullWidth
                    />
                </Grid><Grid item xs={12}>
                <TextField
                    required
                    label="Телефон"
                    name="contact"
                    value={values.contact}
                    onChange={handleChange}
                    fullWidth
                />
            </Grid>
                <Grid item xs={12}>
                    <TextField
                        required
                        label="Ссылка на изображение"
                        name="url"
                        value={values.url}
                        onChange={handleChange}
                        fullWidth
                    />
                </Grid><Grid item xs={12}>
            </Grid>
                <Grid item xs={12}>
                    <Button variant="contained" color="primary"
                            onClick={handleSubmit}
                    >
                        Добавить
                    </Button>
                </Grid>
            </Grid>
        </Box>
    );
};

export default AddAnnouncement;
