import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import AnnouncementCarousel from "./AnnouncementCarousel/AnnouncementCarousel";

const ActiveAnnouncement = ({item, images}) => {
    const {title, body, price, region, username, contact} = item

    return (
        <Fragment>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <h1>{title}</h1>
                    <Box textAlign={'left'} m={1}>
                        <Typography noWrap>{username}</Typography>
                    </Box>
                </Grid>
                <Grid item xs={12}>
                    {!images
                        ? <p>Нет фото</p>
                        : <AnnouncementCarousel
                            images={images}
                            title={title}
                        />
                    }
                </Grid>
                <Grid item xs={12} sm={4}>
                    <Box textAlign={'left'} m={1}>
                        <Typography variant={'h6'} component={'span'}>{body}</Typography>
                    </Box>
                </Grid>
                <Grid item xs={12}>
                    <Box textAlign={'left'} m={1}
                         display={'flex'}
                         flexDirection={'column'}>
                        <Typography variant={'h6'} component={'span'}><strong>Цена: </strong> {price}</Typography>
                        <Typography variant={'h6'} component={'span'}><strong>Регион: </strong> {region}</Typography>
                        <Typography variant={'h6'} component={'span'}><strong>Контакты: </strong>: {contact}
                        </Typography>
                    </Box>
                </Grid>
            </Grid>
        </Fragment>
    );
};

ActiveAnnouncement.propTypes = {
    item: PropTypes.object.isRequired
};

export default ActiveAnnouncement;
