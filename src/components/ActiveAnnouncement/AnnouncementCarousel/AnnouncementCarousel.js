import React from 'react';
import PropTypes from 'prop-types';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {Carousel} from 'react-responsive-carousel';

const AnnouncementCarousel = ({images, title}) => {

    return (
        <Carousel dynamicHeight={true} infiniteLoop={true}>
            {images.map(({img, id}) => (
                <div key={id}>
                    <img src={img} alt={title} style={{
                        maxWidth: '800px'
                    }}/>
                </div>
            ))}
        </Carousel>
    );
};

AnnouncementCarousel.propTypes = {
    images: PropTypes.array.isRequired,
};

export default AnnouncementCarousel;
