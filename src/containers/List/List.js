import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchItems} from "../../store/action/action";
import CardList from "../../components/CardList/CardList";

class List extends Component {


    componentDidMount() {
        const {dispatch} = this.props
        dispatch(fetchItems())
    }


    shouldComponentUpdate(nextProps) {
        return (nextProps.items !== this.props.items)
    }


    render() {
        const {items, loading, error} = this.props

        return (
            <Fragment>
                <CardList
                    items={items}
                    loading={loading}
                    error={error}
                />
            </Fragment>
        );
    }
}

export default connect(state => ({
    items: state.announcement.items,
    images: state.announcement.images,
    loading: state.announcement.loading,
    error: state.announcement.error,
}))(List);
