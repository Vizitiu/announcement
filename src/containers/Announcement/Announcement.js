import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchItem} from "../../store/action/action";
import ActiveAnnouncement from "../../components/ActiveAnnouncement/ActiveAnnouncement";

class Announcement extends Component {

    componentDidMount() {
        const {dispatch, match} = this.props
        dispatch(fetchItem(match.params.id))
    }

    render() {
        const {item, loading, error, images} = this.props

        return (
            <Fragment>
                {error ? <p>Error</p> : null}
                {loading
                    ? <p>Loading ...</p>
                    : <ActiveAnnouncement item={item}
                                          images={images}
                    />
                }
            </Fragment>
        );
    }
}

export default connect(state => ({
    item: state.announcement.item,
    images: state.announcement.images,
    loading: state.announcement.loading,
    error: state.announcement.error,
}))(Announcement);
