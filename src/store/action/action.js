import {FETCH_ITEMS, GET_ITEM, SET_ERROR, SET_LOADING} from "../../constants";
import axios from 'axios'

export const fetchItems = () => async dispatch => {
    dispatch(setLoading())

    try {
        const res = await axios.get('https://rannouncement.firebaseio.com/items.json')
        const item = res.data
        const items = []

        Object.keys(res.data).forEach(key => {
            items.push({
                ...item[key],
                id: key,
            })
        })

        dispatch(getItems(items))
    } catch (e) {
        dispatch(setError())
    }

}

export const fetchItem = item => async dispatch => {
    dispatch(setLoading())

    try {
        const res = await axios.get(`https://rannouncement.firebaseio.com/items/${item}.json`)
        dispatch(getItem(res.data, res.data.images))
    } catch (e) {
        dispatch(setError())
    }

}

export const setLoading = () => ({type: SET_LOADING})

export const setError = () => ({type: SET_ERROR})

export const getItems = items => ({
    type: FETCH_ITEMS,
    items
})

export const getItem = (item, images) => ({
    type: GET_ITEM,
    item,
    images
})
