import {FETCH_ITEMS, GET_ITEM, SET_ERROR, SET_LOADING} from "../../constants";

const STATE = {
    items: [],
    item: {},
    images: [],
    loading: false,
    error: false
}


const announcement = (state = STATE, {type, items, item, images}) => {
    switch (type) {
        case SET_LOADING:
            return {
                ...state, loading: true, error: false
            }
        case SET_ERROR:
            return {
                ...state, loading: false, error: true
            }
        case FETCH_ITEMS:
            return {
                ...state, items, loading: false, error: false
            }
        case GET_ITEM:
            return {
                ...state, item, images, loading: false, error: false
            }
        default:
            return state
    }
}

export default announcement
