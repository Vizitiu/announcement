import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.scss'
import {Provider} from "react-redux";
import {createStore, compose, applyMiddleware} from "redux";
import thunk from 'redux-thunk'
import rootReducer from "./store/reducer/rootReducer";

const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        }) : compose;

const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(thunk))
)

const app = (
        <Provider store={store}>
            <App/>
        </Provider>
)

ReactDOM.render(app, document.getElementById('root'));
